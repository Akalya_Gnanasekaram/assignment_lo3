package Ap.p3.qus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
//import java.util.Scanner;

public class OnlineShopDemo {
	public static void main(String[] args) throws NumberFormatException, IOException {
		// Scanner in = new Scanner(System.in);
		System.out.println("*********** Welcome to our online shop **********\n");
		System.out.println("1.Login\n2.SignUp\n3.Products\nYour purpose?");
		// int log = in.nextInt();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int log = Integer.parseInt(br.readLine());

		switch (log) {
		case 1: {
			System.out.println("\nEnter id: ");
			int id = Integer.parseInt(br.readLine());
			// String id = br.readLine();

			Account onlinedb = Account.getInstance();
			Product onlineprodu = Product.getInstance();
			LineItem onlineproitem = LineItem.getInstance();

			try {
				System.out.println("\nSuccesfully login!");
				onlinedb.readData("1001");		
				System.out.println("1.Product\n2.Delete account");
				int accountwork = Integer.parseInt(br.readLine());
				switch (accountwork) {
				case 1: {
					System.out.println("Our products!\n1.Laptops\n2.Dresses\n3.Bags\nPlease choose one:");
					int prod = Integer.parseInt(br.readLine());
					switch (prod) {
					case 1: {
						onlineprodu.readData("101");
						System.out.println("\n");
						onlineproitem.readData("101");
						System.out.println("\n1.Buy\t2.Cancel");
						int selectitem = Integer.parseInt(br.readLine());
						switch (selectitem) {
						case 1: {
							System.out.println("Your payment:");
							LineItem lappay = new LineItem(100000, 2, 0);
							lappay.printpayment();
							Order lapdetail = new Order("101", "2021-09-08", "2021-10-31", "Jaffna", "200000");
							lapdetail.printorder();
						}
							break;
						case 2: {
							System.out.println("Thankyou, Come again!");
						}
							break;
						default: {
							System.out.println("No more");
						}
						}
					}

						break;
					case 2: {
						onlineprodu.readData("102");
						System.out.println("\n");
						onlineproitem.readData("102");
						System.out.println("\n1.Buy\t2.Cancel");
						int selectitem = Integer.parseInt(br.readLine());
						switch (selectitem) {
						case 1: {
							System.out.println("Your payment:");
							LineItem dresspay = new LineItem(1000, 3, 0);
							dresspay.printpayment();
							Order dressdetail = new Order("102", "2021-08-01", "2021-11-01", "Jaffna", "3000");
							dressdetail.printorder();

							System.out.println("1.Pay\t2.Not Pay");
							/*
							 * int pay=Integer.parseInt(br.readLine()); switch(pay) { case 1:{
							 * System.out.println("Successfully Paid!"); } break; case 2:{
							 * System.out.println("Pending pay.."); } break; default:{
							 * System.out.println("Invalied"); }}
							 */
						}
							break;
						case 2: {
							System.out.println("Thankyou, Come again!");

						}
						}
					}
						break;
					case 3: {
						onlineprodu.readData("103");
						System.out.println("\n");
						onlineproitem.readData("103");
						System.out.println("\n1.Buy\t2.Cancel");
						int selectitem = Integer.parseInt(br.readLine());
						switch (selectitem) {
						case 1: {
							System.out.println("Your payment:");
							LineItem bagpay = new LineItem(1500, 2, 0);
							bagpay.printpayment();
							Order bagdetail = new Order("102", "2021-06-08", "2021-11-01", "Jaffna", "3000");
							bagdetail.printorder();
						}
							break;
						case 2: {
							System.out.println("Thankyou, Come again!");
						}

						}
					}
						break;
					default: {
						System.out.println("invalid!");
					}
					}

				}
					break;
				case 2: {
					onlinedb.deleteData("1007");
					System.out.println("Successfully your account deleted!");
				}
					break;
				default: {
					System.out.println("No more");
				}
				}

			} catch (SQLException e) {
				System.out.println("Login failed!");
				e.printStackTrace();
			}
		}
			break;
		case 2: {
			Account onlinedb = Account.getInstance();
			try {
				onlinedb.insertData("1007", "milux", "india", "2020-01-07", "2023-01-07");
				System.out.println("Successfully create your online account!");
				onlinedb.readData("1007");
				// new
				Product onlineprodu = Product.getInstance();
				LineItem onlineproitem = LineItem.getInstance();
				System.out.println("1.Product\n2.Exit");
				int accountwork = Integer.parseInt(br.readLine());
				switch (accountwork) {
				case 1: {
					System.out.println("Our products!\n1.Laptops\n2.Dresses\n3.Bags\nPlease choose one:");
					int prod = Integer.parseInt(br.readLine());
					switch (prod) {
					case 1: {
						// System.out.println("Lap");
						onlineprodu.readData("101");
						System.out.println("\n");
						onlineproitem.readData("101");
						System.out.println("\n1.Buy\t2.Cancel");
						int selectitem = Integer.parseInt(br.readLine());

						switch (selectitem) {
						case 1: {
							System.out.println("Your payment:");
							LineItem lappay = new LineItem(100000, 2, 0);
							lappay.printpayment();
							Order lapdetail = new Order("101", "2021-09-08", "2021-10-31", "Jaffna", "200000");
							lapdetail.printorder();
						}
							break;
						case 2: {
							System.out.println("Thankyou, Come again!");
						}
							break;
						default: {
							System.out.println("No more");
						}
						}

					}
						break;
					case 2: {
						// System.out.println("Dress");

						onlineprodu.readData("102");
						System.out.println("\n");
						onlineproitem.readData("102");
						System.out.println("\n1.Buy\t2.Cancel");
						int selectitem = Integer.parseInt(br.readLine());
						switch (selectitem) {
						case 1: {
							System.out.println("Your payment:");
							LineItem dresspay = new LineItem(1000, 3, 0);
							dresspay.printpayment();
							Order dressdetail = new Order("102", "2021-08-01", "2021-11-01", "Jaffna", "3000");
							dressdetail.printorder();

							System.out.println("1.Pay\t2.Not Pay");
							/*
							 * int pay=Integer.parseInt(br.readLine()); switch(pay) { case 1:{
							 * System.out.println("Successfully Paid!"); } break; case 2:{
							 * System.out.println("Pending pay.."); } break; default:{
							 * System.out.println("Invalied"); }}
							 */
						}
							break;
						case 2: {
							System.out.println("Thankyou, Come again!");

						}
						}

					}
						break;
					case 3: {
						// System.out.println("bag");
						onlineprodu.readData("103");
						System.out.println("\n");
						onlineproitem.readData("103");
						System.out.println("\n1.Buy\t2.Cancel");
						int selectitem = Integer.parseInt(br.readLine());
						switch (selectitem) {
						case 1: {
							System.out.println("Your payment:");
							LineItem bagpay = new LineItem(1500, 2, 0);
							bagpay.printpayment();
							Order bagdetail = new Order("102", "2021-06-08", "2021-11-01", "Jaffna", "3000");
							bagdetail.printorder();
						}
						}
					}
						break;
					default: {
						System.out.println("Invalid!");
					}
					}
				}
					break;
				case 2: {
					System.out.println("Exited!");
				}
					break;
				default: {
					System.out.println("Invalid!");
				}

				}

				// neww

			} catch (SQLException e) {
				System.out.println("Insert failed!");
				e.printStackTrace();
			}
		}
			break;
		case 3: {
			System.out.println("Welcome to our product section!");
			// neww
		//	Product onlineprodu = Product.getInstance();
			System.out.println("1.Product\n2.Exit");
			int accountwork = Integer.parseInt(br.readLine());
			switch (accountwork) {
			case 1: {
				System.out.println("Our products!\n1.Laptops\n2.Dresses\n3.Bags\nPlease choose one:");
				int prod = Integer.parseInt(br.readLine());
				
				switch (prod) {
				case 1: {
					System.out.println("Laptops");
				}
					break;
				case 2: {
					System.out.println("Dresses");
				}
					break;
				case 3: {
					System.out.println("Bags");
				}
					break;
				default: {
					System.out.println("Invalid!");
				}
				}

			}
				break;
			case 2: {
				System.out.println("Exited!");
			}
				break;
			default: {
				System.out.println("Invalid!");
			}

			}

			// new end
		}
			break;
		default: {
			System.out.println("no more");
		}

		}
	}
}
