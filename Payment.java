package Ap.p3.qus;

import java.io.IOException;

public class Payment {
	public static void main(String[] args) throws IOException {

		LineItem lappay = new LineItem(100000, 2, 0);
		lappay.printpayment();
		
		LineItem dresspay = new LineItem(1000, 3, 0);
		dresspay.printpayment();
		
		LineItem bagpay = new LineItem(1500, 2, 0);
		bagpay.printpayment();
		
		
		Order lapdetail=new Order("101", "2021-09-08", "2021-10-31", "Jaffna","200000");
		lapdetail.printorder();
		
		Order dressdetail=new Order("102", "2021-08-01", "2021-11-01", "Jaffna","3000");
		dressdetail.printorder();
		
		Order bagdetail=new Order("102", "2021-06-08", "2021-11-01", "Jaffna","3000");
		bagdetail.printorder();
		
		
	}

}
