package Ap.p3.qus;

enum UserState {
	NEW, ACTIVE, BLOCKED, BANNED;
}

public class WebUser {
	private static final String NEW = "NEW";
	private static final String ACTIVE = "OLD";
	private static final String BLOCKED = "BLOCKED";
	private static final String BANNED = "BANNED";
	private String id;
	private String password;

	public WebUser(String idnum, String passwordnum) {
		this.id = idnum;
		this.setPassword(passwordnum);
	}

	public void print() {
		switch (id) {
		case NEW: {
			System.out.println("New Account");
		}
			break;

		case ACTIVE: {
			System.out.println("Active Account");
		}
			break;
		case BLOCKED: {
			System.out.println("Blocked Account");
		}
			break;
		case BANNED: {
			System.out.println("Banned Account");
		}
		}

	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
