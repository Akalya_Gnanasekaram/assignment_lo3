package Ap.p3.qus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LineItem {

	public double payment;
	private double price;
	private double quantity;

	
	private static LineItem onlinedb;

	LineItem() {

	}

	public static LineItem getInstance() {
		if (onlinedb == null) {
			onlinedb = new LineItem();
		}
		return onlinedb;
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection dbconnect = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		dbconnect = DriverManager.getConnection("JDBC:mysql://localhost:3306/online_shopping", "root", "root");
		return dbconnect;
	}

	// create LineItems from database
	public void insertData(String id,String name, String types, int quantity, int price) throws SQLException {
		Connection dbconnect = null;
		PreparedStatement dbstatement = null;
		try {
			dbconnect = this.getConnection();

			dbstatement = dbconnect.prepareStatement("INSERT INTO lineitem (id,name,types,quantity,price) VALUES(?,?,?,?,?)");
			dbstatement.setString(1, id);
			dbstatement.setString(2, name);
			dbstatement.setString(3, types);
			dbstatement.setInt(4, quantity);
			dbstatement.setInt(5, price);
			dbstatement.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (dbconnect != null) {
				dbconnect.close();
			}
			if (dbstatement != null) {
				dbstatement.close();
			}
		}

	}
	// read LineItems from database
	public void readData(String searchText) throws SQLException {
		Connection dbconnect = null;
		PreparedStatement dbstatement = null;
		ResultSet resultSet = null;
		
		try {
			dbconnect = this.getConnection();
			dbstatement = dbconnect.prepareStatement("SELECT * FROM lineitem WHERE id=?");
			dbstatement.setString(1, searchText);
			resultSet = dbstatement.executeQuery();
			System.out.println("Id\tName\tTypes\tQuantity\tPrice");
			while (resultSet.next()) {
				System.out.println(resultSet.getString(1) + "\t" + resultSet.getString(2)+"\t" + resultSet.getString(3)+"\t" + resultSet.getString(4)+"\t\t" + resultSet.getString(5));
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (dbconnect != null) {
				dbconnect.close();
			}
			if (dbstatement != null) {
				dbstatement.close();
			}
		}
	}

	//get set
	public double getPrice() {
		return price;
	}

	public void setPrice(double prc) {
		this.price = prc;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double qutty) {
		this.quantity = qutty;
	}
	public double getPayment() {
		return payment;
	}

	public void setPayment(double pay) {
		this.payment = pay;
	}
	
	///
	public LineItem(double prc, double qutty,double pay) {
		price=prc;
		quantity=qutty;
		payment = prc *qutty;
	}

	public void printpayment() {
		System.out.println("Pricee: "+price+"\tQuantity: "+quantity+"\tPayment: "+payment);
	}
	
	
	
	
	
	
	
	
	    }